const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validate = require('mongoose-validator');

var emailValidator = [
    validate({
        validator: 'isEmail',
        passIfEmpty: true,
        message: 'Please enter a valid email address.'
    }),
]

var passwordValidator = [
    validate({
        validator: 'matches',
        arguments: /(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        message: 'Your password must contain at least 6 character, a capital letter and a number.'
    }),
]

const userSchema = new Schema({
    email: { type: String, required: true, trim: true, unique: true, validate: emailValidator },
    password: { type: String, required: false, trim: true, validate: passwordValidator },
});

module.exports = mongoose.model('User', userSchema);