const userTypeDefs = require('./userTypeDefs');
const userQueryResolvers = require('./queryResolvers');
const userMutationResolvers = require('./mutationResolvers');

module.exports = {
	userTypeDefs,
	userQueryResolvers,
	userMutationResolvers
}