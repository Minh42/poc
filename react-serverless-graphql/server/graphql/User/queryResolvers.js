const userQueryResolvers = {
    Query: {
        users: async (_, args, {models}) => {
            return await models.User.find();
        },
        user: async(_, {id}, {models}) => {
            return await models.User.findOne({_id: id});
        }
    }
}

module.exports = userQueryResolvers;