const { gql } = require('apollo-server-lambda');

const userTypeDefs = gql`
type User {
    _id: ID!
    email: String!
}

extend type Query {
    users: [User!]!
    user(id: ID!): User
}

extend type Mutation {
    signup(email: String!, password: String!): Boolean!
    signin(email: String!, password: String!): Boolean!
}
`;

module.exports = userTypeDefs;