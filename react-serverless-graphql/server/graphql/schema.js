const merge = require('lodash.merge');
const { gql } = require('apollo-server-lambda');
const { makeExecutableSchema } = require('graphql-tools');
const { userTypeDefs, userQueryResolvers, userMutationResolvers } = require('./User/index');

/**
 * We must define a root type so that our server knows where to
 * look when we query the server i.e. in the "root" types.
 */
const Root = `
	type Query {
		dummy: String
	}

	type Mutation {
		dummy: String
	}

	type Subscription {
		dummy: String
	}

	schema {
		query: Query
		mutation: Mutation
		subscription: Subscription
	}
`

const resolvers = merge(
	{},
	/**
	 * Queries
	 */
	userQueryResolvers,
	// bookingQueryResolvers,
	/**
	 * Mutations
	 */
	userMutationResolvers,
	// bookingMutationResolvers,
	/**
	 * Subscriptions
	 */
)

/**
 * Declare the schema which the will hold our GraphQL types and
 * resolvers.
 */
const schema = makeExecutableSchema({
  typeDefs: [Root, userTypeDefs],
  resolvers,
});

module.exports = schema;