const { ApolloServer, gql } = require('apollo-server-lambda');
const schema = require('./graphql/schema');
// const keys = require('./config/keys');
const models = require('./models');
const db = require('./db/connection');

const server = new ApolloServer({
  schema,
  context: ({ event, context }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
    models,
    // keys
  }),
});

exports.graphqlHandler = server.createHandler({
  cors: {
    origin: true,
    credentials: true,
  },
});