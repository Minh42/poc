(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./db/connection.js":
/*!**************************!*\
  !*** ./db/connection.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mongoose = __webpack_require__(/*! mongoose */ "mongoose");

var connection = {};
console.log(process.env.MONGO_URI);

module.exports = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
  var db;
  return _regenerator2.default.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (!connection.isConnected) {
            _context.next = 3;
            break;
          }

          console.log('=> using existing database connection');
          return _context.abrupt('return');

        case 3:

          console.log('=> using new database connection');
          _context.next = 6;
          return mongoose.connect(mongoURI, { useCreateIndex: true, useNewUrlParser: true });

        case 6:
          db = _context.sent;

          connection.isConnected = db.connections[0].readyState;

        case 8:
        case 'end':
          return _context.stop();
      }
    }
  }, _callee, undefined);
}));

/***/ }),

/***/ "./graphql/User/index.js":
/*!*******************************!*\
  !*** ./graphql/User/index.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

var userTypeDefs = __webpack_require__(/*! ./userTypeDefs */ "./graphql/User/userTypeDefs.js");
var userQueryResolvers = __webpack_require__(/*! ./queryResolvers */ "./graphql/User/queryResolvers.js");
var userMutationResolvers = __webpack_require__(/*! ./mutationResolvers */ "./graphql/User/mutationResolvers.js");

module.exports = {
	userTypeDefs: userTypeDefs,
	userQueryResolvers: userQueryResolvers,
	userMutationResolvers: userMutationResolvers
};

/***/ }),

/***/ "./graphql/User/mutationResolvers.js":
/*!*******************************************!*\
  !*** ./graphql/User/mutationResolvers.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userMutationResolvers = {
    Mutation: {
        signup: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_, _ref2, _ref3) {
                var email = _ref2.email,
                    password = _ref2.password;
                var models = _ref3.models,
                    keys = _ref3.keys;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                console.log('im here in signup');
                                // const user = await new models.User({email, password}).save();
                                console.log(models);
                                return _context.abrupt('return', true);

                            case 3:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined);
            }));

            return function signup(_x, _x2, _x3) {
                return _ref.apply(this, arguments);
            };
        }(),
        signin: function () {
            var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_, _ref5, _ref6) {
                var email = _ref5.email,
                    password = _ref5.password;
                var req = _ref6.req,
                    res = _ref6.res,
                    models = _ref6.models,
                    keys = _ref6.keys;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                console.log('im in signin');
                                return _context2.abrupt('return', true);

                            case 2:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined);
            }));

            return function signin(_x4, _x5, _x6) {
                return _ref4.apply(this, arguments);
            };
        }()
    }
};

module.exports = userMutationResolvers;

/***/ }),

/***/ "./graphql/User/queryResolvers.js":
/*!****************************************!*\
  !*** ./graphql/User/queryResolvers.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userQueryResolvers = {
    Query: {
        users: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_, args, _ref2) {
                var models = _ref2.models;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return models.User.find();

                            case 2:
                                return _context.abrupt("return", _context.sent);

                            case 3:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, undefined);
            }));

            return function users(_x, _x2, _x3) {
                return _ref.apply(this, arguments);
            };
        }(),
        user: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_, _ref4, _ref5) {
                var id = _ref4.id;
                var models = _ref5.models;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return models.User.findOne({ _id: id });

                            case 2:
                                return _context2.abrupt("return", _context2.sent);

                            case 3:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined);
            }));

            return function user(_x4, _x5, _x6) {
                return _ref3.apply(this, arguments);
            };
        }()
    }
};

module.exports = userQueryResolvers;

/***/ }),

/***/ "./graphql/User/userTypeDefs.js":
/*!**************************************!*\
  !*** ./graphql/User/userTypeDefs.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _taggedTemplateLiteral2 = __webpack_require__(/*! babel-runtime/helpers/taggedTemplateLiteral */ "babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\ntype User {\n    _id: ID!\n    email: String!\n}\n\nextend type Query {\n    users: [User!]!\n    user(id: ID!): User\n}\n\nextend type Mutation {\n    signup(email: String!, password: String!): Boolean!\n    signin(email: String!, password: String!): Boolean!\n}\n'], ['\ntype User {\n    _id: ID!\n    email: String!\n}\n\nextend type Query {\n    users: [User!]!\n    user(id: ID!): User\n}\n\nextend type Mutation {\n    signup(email: String!, password: String!): Boolean!\n    signin(email: String!, password: String!): Boolean!\n}\n']);

__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _require = __webpack_require__(/*! apollo-server-lambda */ "apollo-server-lambda"),
    gql = _require.gql;

var userTypeDefs = gql(_templateObject);

module.exports = userTypeDefs;

/***/ }),

/***/ "./graphql/schema.js":
/*!***************************!*\
  !*** ./graphql/schema.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

var merge = __webpack_require__(/*! lodash.merge */ "lodash.merge");

var _require = __webpack_require__(/*! apollo-server-lambda */ "apollo-server-lambda"),
    gql = _require.gql;

var _require2 = __webpack_require__(/*! graphql-tools */ "graphql-tools"),
    makeExecutableSchema = _require2.makeExecutableSchema;

var _require3 = __webpack_require__(/*! ./User/index */ "./graphql/User/index.js"),
    userTypeDefs = _require3.userTypeDefs,
    userQueryResolvers = _require3.userQueryResolvers,
    userMutationResolvers = _require3.userMutationResolvers;

/**
 * We must define a root type so that our server knows where to
 * look when we query the server i.e. in the "root" types.
 */


var Root = '\n\ttype Query {\n\t\tdummy: String\n\t}\n\n\ttype Mutation {\n\t\tdummy: String\n\t}\n\n\ttype Subscription {\n\t\tdummy: String\n\t}\n\n\tschema {\n\t\tquery: Query\n\t\tmutation: Mutation\n\t\tsubscription: Subscription\n\t}\n';

var resolvers = merge({},
/**
 * Queries
 */
userQueryResolvers,
// bookingQueryResolvers,
/**
 * Mutations
 */
userMutationResolvers);

/**
 * Declare the schema which the will hold our GraphQL types and
 * resolvers.
 */
var schema = makeExecutableSchema({
	typeDefs: [Root, userTypeDefs],
	resolvers: resolvers
});

module.exports = schema;

/***/ }),

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

var _require = __webpack_require__(/*! apollo-server-lambda */ "apollo-server-lambda"),
    ApolloServer = _require.ApolloServer,
    gql = _require.gql;

var schema = __webpack_require__(/*! ./graphql/schema */ "./graphql/schema.js");
// const keys = require('./config/keys');
var models = __webpack_require__(/*! ./models */ "./models/index.js");
var db = __webpack_require__(/*! ./db/connection */ "./db/connection.js");

var server = new ApolloServer({
  schema: schema,
  context: function context(_ref) {
    var event = _ref.event,
        _context = _ref.context;
    return {
      headers: event.headers,
      functionName: _context.functionName,
      event: event,
      context: _context,
      models: models
      // keys
    };
  }
});

exports.graphqlHandler = server.createHandler({
  cors: {
    origin: true,
    credentials: true
  }
});

/***/ }),

/***/ "./models/index.js":
/*!*************************!*\
  !*** ./models/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

var User = __webpack_require__(/*! ./user.model */ "./models/user.model.js");

var models = {
    User: User
};

module.exports = models;

/***/ }),

/***/ "./models/user.model.js":
/*!******************************!*\
  !*** ./models/user.model.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! source-map-support/register */ "source-map-support/register");

var mongoose = __webpack_require__(/*! mongoose */ "mongoose");
var Schema = mongoose.Schema;
var validate = __webpack_require__(/*! mongoose-validator */ "mongoose-validator");

var emailValidator = [validate({
    validator: 'isEmail',
    passIfEmpty: true,
    message: 'Please enter a valid email address.'
})];

var passwordValidator = [validate({
    validator: 'matches',
    arguments: /(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    message: 'Your password must contain at least 6 character, a capital letter and a number.'
})];

var userSchema = new Schema({
    email: { type: String, required: true, trim: true, unique: true, validate: emailValidator },
    password: { type: String, required: false, trim: true, validate: passwordValidator }
});

module.exports = mongoose.model('User', userSchema);

/***/ }),

/***/ "apollo-server-lambda":
/*!***************************************!*\
  !*** external "apollo-server-lambda" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("apollo-server-lambda");

/***/ }),

/***/ "babel-runtime/helpers/asyncToGenerator":
/*!*********************************************************!*\
  !*** external "babel-runtime/helpers/asyncToGenerator" ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/asyncToGenerator");

/***/ }),

/***/ "babel-runtime/helpers/taggedTemplateLiteral":
/*!**************************************************************!*\
  !*** external "babel-runtime/helpers/taggedTemplateLiteral" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/taggedTemplateLiteral");

/***/ }),

/***/ "babel-runtime/regenerator":
/*!********************************************!*\
  !*** external "babel-runtime/regenerator" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/regenerator");

/***/ }),

/***/ "graphql-tools":
/*!********************************!*\
  !*** external "graphql-tools" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("graphql-tools");

/***/ }),

/***/ "lodash.merge":
/*!*******************************!*\
  !*** external "lodash.merge" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("lodash.merge");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "mongoose-validator":
/*!*************************************!*\
  !*** external "mongoose-validator" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose-validator");

/***/ }),

/***/ "source-map-support/register":
/*!**********************************************!*\
  !*** external "source-map-support/register" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("source-map-support/register");

/***/ })

/******/ })));
//# sourceMappingURL=index.js.map