import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { ApolloClient } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const client = new ApolloClient({
	link: new HttpLink({ uri: 'http://localhost:3000/graphql' }),
	cache: new InMemoryCache(),
	credentials: 'include'
  });

// const client = new ApolloClient({
//   link: new HttpLink({ uri: 'https://hjrne2cwn9.execute-api.eu-west-3.amazonaws.com/dev/graphql' }),
//   cache: new InMemoryCache(),
// });

ReactDOM.render(
	<ApolloProvider client={client}>
		<App />,
	</ApolloProvider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
