import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Signup from './components/Signup';

class App extends Component { 
    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        <Route exact path="/" component={Signup} />
                    </Switch>
                </div>
            </Router>
        )
    }

}

export default App;