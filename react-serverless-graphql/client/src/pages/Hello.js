import React from 'react';
import gql from "graphql-tag";
import { Query } from "react-apollo";

const HELLO = gql`
	query {
		hello
	}
`;

const Hello = () => (
  <Query query={HELLO}>
    {({ loading, error, data }) => {
		console.log(error)
		console.log(data);
      return (
			<div>
				Dude
			</div>
      );
    }}
  </Query>
);

export default Hello;