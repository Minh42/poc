import gql from "graphql-tag";

export const SIGNUP = gql`
  mutation SignupMutation($email: String!, $password: String!) {
    signup(email: $email, password: $password)
  }
`;

export const SIGNIN = gql`
  mutation SigninMutation($email: String!, $password: String!) {
    signin(email: $email, password: $password)
  }
`;
