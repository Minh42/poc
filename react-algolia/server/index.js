import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import validationSchema from './utils/validationSchema';
import searchClient from './utils/searchClient';

const app = express();
const PORT = process.env.PORT || 8080;
const middlewares = [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    cors({ credentials: true, origin: "http://localhost:3000" }),
]

app.use(middlewares);

app.post('/api/1/movies', (req, res) => {
    validationSchema
        .isValid(req.body.data)
        .then(function (valid) {
            if (valid) {
                const index = searchClient.initIndex('movies');
                let object = req.body.data;
                let actors = [];
                let actorFacets = [];
                for (let key in object) {
                    if (key === 'actors') {
                        for (let elem of object['actors']) {
                            actors.push(elem.actor);
                            actorFacets.push(elem.actorFacet + '|' + elem.actor);
                        }
                    }
                }
                delete object['actors'];
                object = [{ ...object, actors: actors, actor_facets: actorFacets }];
                index
                    .addObjects(object)
                    .then(result => {
                        res.send(result.objectIDs);
                    })
                    .catch(err => console.error(err));
            }
        });
})

app.listen(PORT, () => {
    console.log(`App running at http://localhost:${PORT}`);
})