import path from 'path';
import fs from 'fs';
import searchClient from '../utils/searchClient';
import StreamArray from 'stream-json/streamers/StreamArray';

// Connect to Algolia Index with API client
const index = searchClient.initIndex('movies');

// Sending records in batches to reduce network calls and speeds up indexing
// https://www.algolia.com/doc/guides/sending-and-managing-data/send-and-update-your-data/how-to/sending-records-in-batches/

const stream = fs.createReadStream(path.resolve(__dirname, 'records.json')).pipe(StreamArray.withParser());
let chunks = [];

stream
    .on('data', ({ value }) => {
        chunks.push(value);
        if (chunks.length === 200) {
            stream.pause();
            index
                .addObjects(chunks)
                .then(res => {
                    chunks = [];
                    stream.resume();
                })
                .catch(err => console.error(err));
        }
    })
    .on('end', () => {
        if (chunks.length) {
            index.addObjects(chunks).catch(err => console.error(err));
            console.log('All records were added to Algolia servers.');
        }
    })
    .on('error', err => console.error(err));

index.setSettings({
    searchableAttributes: [ /* create searchable attributes */
        'title,alternative_titles',
        'actors,year'
    ],
    replicas: [ /* create replicas of the index */
        'movies_rating_asc',
        'movies_rating_desc',
    ],
    attributesForFaceting: [ /* create a facet */
        'rating',
        'searchable(genre)'
    ]
});

// Set replicas settings for sorting purposes
const replicaIndexMoviesAsc = searchClient.initIndex('movies_rating_asc');

replicaIndexMoviesAsc.setSettings({
    ranking: [
        "asc(rating)",
        "typo",
        "geo",
        "words",
        "filters",
        "proximity",
        "attribute",
        "exact",
        "custom"
    ],
    attributesForFaceting: [ /* create a facet */
        'rating',
        'searchable(genre)'
    ]
});

const replicaIndexMoviesDesc = searchClient.initIndex('movies_rating_desc');

replicaIndexMoviesDesc.setSettings({
    ranking: [
        "desc(rating)",
        "typo",
        "geo",
        "words",
        "filters",
        "proximity",
        "attribute",
        "exact",
        "custom"
    ],
    attributesForFaceting: [ /* create a facet */
        'rating',
        'searchable(genre)'
    ]
});