require('dotenv').config();
import algoliasearch from 'algoliasearch';

const searchClient = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_ADMIN_KEY);

export default searchClient;