import * as yup from 'yup';

const validationSchema = yup.object({
    title: yup
        .string()
        .required()
        .max(60),
    alternativeTitles: yup.array().of(
        yup.string().required().max(60)
    ),
    year: yup
        .number()
        .integer()
        .required()
        .strict()
        .min(1900)
        .max(new Date().getFullYear()),
    image: yup
        .string()
        .required(),
    color: yup
        .string()
        .required()
        .matches(
        /^#([0-9a-f]{3}|[0-9a-f]{6})$/i,
    ),
    score: yup
        .number()
        .positive()
        .required(),
    rating: yup
        .number()
        .integer()
        .strict()
        .required()
        .min(0)
        .max(5),
    actors: yup.array().of(
        yup.object({
            actor: yup.string().required(),
            actorFacet: yup.string().required()
        })
    ),
    genre: yup.array().of(
        yup.string().required().max(30)
    ),
});

export default validationSchema;