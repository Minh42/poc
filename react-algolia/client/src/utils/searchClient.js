import algoliasearch from 'algoliasearch';

const searchClient = algoliasearch(process.env.REACT_APP_ALGOLIA_APP_ID, process.env.REACT_APP_ALGOLIA_ADMIN_KEY);

export default searchClient;