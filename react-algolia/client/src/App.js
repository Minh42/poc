import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomePage from './pages/HomePage';
import FormPage from './pages/FormPage';
import GlobalStyle from './styles/globalStyle';
import './App.css';

const App = () => {
  return (
    <Router>
      <GlobalStyle />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/form" component={FormPage} />
      </Switch>
    </Router>
  );
}

export default App;
