import React from 'react';
import styled from 'styled-components';
import { Stats, Pagination, RefinementList, SortBy, RatingMenu } from 'react-instantsearch-dom';
import Box from '../atoms/Box';
import Button from '../atoms/Button';
import CustomHits from './CustomHits';
import { Link } from "react-router-dom";

const MainWrapper = styled.main`
    display: flex;
    height: calc(100vh - 20rem);
`

const LeftPanel = styled.div`
    flex: 0 0 22%;
    height: 100%;
    padding: 1rem;
`

const Content = styled.div`
    flex: 1;
    padding: 1rem;
`

const Wrapper = styled.div`
    display: flex;
    justify-content: space-between;
`
const Main = () => {
    return (
        <MainWrapper>
            <LeftPanel>
                <RefinementList
                    attribute="genre"
                    showMore
                    searchable
                    translations={{
                        showMore(expanded) {
                            return expanded ? 'Show less' : 'Show more';
                        },
                        noResults: 'Aucun résultat',
                        placeholder: 'Rechercher...',
                    }}
                />
                <Box>
                    <RatingMenu attribute="rating" />
                </Box>
                <Link to="/form">
                    <Button >Ajouter un film</Button>
                </Link>
            </LeftPanel>
            <Content>
                <Wrapper>
                    <Stats
                        translations={{
                            stats(nbHits, timeSpentMS) {
                                return `${nbHits} résultats trouvés en ${timeSpentMS}ms`;
                            },
                        }}
                    />
                    <SortBy
                        defaultRefinement="movies"
                        items={[
                            { value: 'movies', label: 'Par défaut' },
                            { value: 'movies_rating_asc', label: 'Moins bien noté' },
                            { value: 'movies_rating_desc', label: 'Mieux noté' },
                        ]}
                    />
                </Wrapper>
                <Box>
                    <CustomHits />
                </Box>
                <Pagination />
            </Content>
        </MainWrapper>
    )
};

export default Main;