import React, { Component } from 'react';
import searchClient from '../utils/searchClient'
import { InstantSearch, Configure } from 'react-instantsearch-dom';
import styled from 'styled-components';
import Logo from '../atoms/Logo';
import Typography from '../atoms/Typography';
import Box from '../atoms/Box';
import Autocomplete from '../atoms/Autocomplete';
import backgroundImg from '../assets/background-img.jpg';

const HeaderWrapper = styled.header`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height:20rem;
    background-image: url(${backgroundImg});
    background-size: cover;
`

class Header extends Component {
    render() {
        return (
            <HeaderWrapper>
                <Logo />
                <Box>
                    <Typography>Tous vos films préférés</Typography>
                </Box>
                <InstantSearch indexName="movies" searchClient={searchClient}>
                    <Configure
                        hitsPerPage={5}
                    />
                    <Autocomplete
                        onSuggestionSelected={this.props.onSuggestionSelected}
                        onSuggestionCleared={this.props.onSuggestionCleared}
                    />
                </InstantSearch>
            </HeaderWrapper>
        )
    }
};

export default Header;