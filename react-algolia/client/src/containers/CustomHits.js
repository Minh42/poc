import React from 'react';
import { connectHits, Highlight } from 'react-instantsearch-dom';
import styled from 'styled-components';
import Rating from '../atoms/Rating';
import Avatar from '../atoms/Avatar';
import Box from '../atoms/Box';

const Cards = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-auto-rows: auto;
    grid-gap: 1rem;
`

const Card = styled.div`
    display: flex;
    overflow: auto;
`

const CardContent = styled.div`
    padding: 0.5rem;
`

const Hits = ({ hits }) => {
    return (
        <Cards>
            {hits.map(hit => (
                <Card key={hit.objectID} >
                    <img src={hit.image} alt={hit.name} />
                    <CardContent>
                        <h3><Highlight attribute="title" hit={hit} /> ({hit.year})</h3>
                        <Rating
                            rating={hit.rating}
                        />
                        <Box>
                            <h4>Casting: acteurs principaux</h4>
                        </Box>
                        {hit.actor_facets.map(actor => (
                            <Avatar src={actor.split('|')[0]} alt={hit.name} />
                        ))}
                    </CardContent>
                </Card>
            ))}
        </Cards>
    )
};

const CustomHits = connectHits(Hits);

export default CustomHits;