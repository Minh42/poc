import { createGlobalStyle } from 'styled-components';
import colors from './colors';
import { respondTo } from './mixins';

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Hind:400,500,600&display=swap');
  
  *,
  *::before,
  *::after {
    margin: 0; /* by default browsers apply a certain margin or padding */
    padding: 0; 
    box-sizing: inherit
  }

  html {
    font-size: 62.5%; /* 1rem = 10px, 10/16 = 62,5% */

    ${respondTo.md`
      font-size: 56.25%; /* 1rem = 9px, 9/16 = 56.25% */
	  `}

    ${respondTo.md`
      font-size: 50%; /* 1 rem = 8px, 8/16 = 50% */
	  `}

    ${respondTo.lg`
      font-size: 75%; /* 1rem = 12, 12/16 = 75% */
	  `}
  }

  body {
    box-sizing: border-box;
    font-family: 'Hind', sans-serif;
    color: ${colors.black};
    background: ${colors.background};
  }

  ::selection {
    background-color: ${colors.primary};
    color: ${colors.white};
  }
`;

export default GlobalStyle;
