export default {
    primary: '#5468ff',
    black: '#192A3E',
    white: '#fff',
    darkBlueText: '#3a416f',
    darkGrayText: '#5d6494',
    divider: '#EBEFF2',
    negativeRed: '#FF3C5F'
};
