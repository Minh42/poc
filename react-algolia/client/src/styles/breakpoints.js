const breakpoints = {
    xs: '37.5rem',
    sm: '60rem',
    md: '80rem',
    lg: '120rem'
};

export default breakpoints;