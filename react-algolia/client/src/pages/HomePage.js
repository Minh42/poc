import React, { Component } from 'react';
import 'instantsearch.css/themes/algolia.css';
import searchClient from '../utils/searchClient';
import { InstantSearch, Configure, connectSearchBox } from 'react-instantsearch-dom';
import Header from '../containers/Header';
import Main from '../containers/Main';

const VirtualSearchBox = connectSearchBox(() => null);

class HomePage extends Component {
    state = {
        query: '',
    };

    onSuggestionSelected = (_, { suggestion }) => {
        this.setState({
            query: suggestion.title,
        });
    };

    onSuggestionCleared = () => {
        this.setState({
            query: '',
        });
    };

    render() {
        const { query } = this.state;
        return (
            <InstantSearch indexName="movies" searchClient={searchClient}>
                <VirtualSearchBox defaultRefinement={query} />
                <Configure
                    hitsPerPage={15}
                />
                <Header
                    onSuggestionSelected={this.onSuggestionSelected}
                    onSuggestionCleared={this.onSuggestionCleared}
                />
                <Main />
            </InstantSearch>
        );
    }
};

export default HomePage;