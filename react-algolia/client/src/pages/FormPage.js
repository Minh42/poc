import React from 'react';
import styled from 'styled-components';
import { Formik, Form, Field, FieldArray } from "formik";
import Button from '../atoms/Button';
import Icon from '../atoms/Icon';
import FormInput from '../atoms/FormInput';
import FormLabel from '../atoms/FormLabel';
import Minus from '../assets/minus-solid.svg';
import Plus from '../assets/plus-solid.svg';
import validationSchema from '../utils/validationSchema';
import axios from 'axios';

const FormWrapper = styled(Form) `
    display: flex;
    height: 100vh;
`

const LeftPanel = styled.div`
    flex: 1;
    width: 100%;
    height: auto;
    padding: 2rem;
`

const RightPanel = styled.div`
    flex: 1;
    width: 100%;
    height: auto;
    padding: 2rem;
`

const FormControl = styled.div`
    &:not(:last-child) {
        margin-bottom: 1rem;
    }
`
const ActionsControl = styled.div`
    display: flex;
    flex-direction: row;    
`

const FormPage = () => {
    return (
        <Formik
            initialValues={{ title: '', alternativeTitles: [], year: '', image: '', color: '', score: '', rating: '', actors: [{ actor: '', actorFacet: '' }], genre: [] }}
            validationSchema={validationSchema}
            onSubmit={async (data, { setSubmitting, resetForm }) => {
                try {
                    const res = await axios.post('http://localhost:8080/api/1/movies', { data });
                    const izitoast = require('izitoast');
                    izitoast.success({
                        message: 'ObjectId' + res.data[0] + 'a été ajouté',
                        position: 'topRight'
                    });
                } catch (err) {
                    throw err;
                }
                setSubmitting(false);
                resetForm();
            }}
        >
            {({ values, isSubmitting }) => (
                <FormWrapper>
                    <LeftPanel>
                        <FormControl>
                            <Field name="title" placeholder="Titre" label="Titre" type="input" as={FormInput} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Titres alternatives</FormLabel>
                            <FieldArray
                                name="alternativeTitles"
                                render={arrayHelpers => (
                                    <>
                                    {values.alternativeTitles && values.alternativeTitles.length > 0 ? (
                                        values.alternativeTitles.map((title, index) => (
                                            <ActionsControl key={index}>
                                                <Field name={`alternativeTitles.${index}`} type="input" as={FormInput} />
                                                <Icon src={Minus} alt="minus" onClick={() => arrayHelpers.remove(index)} />
                                                <Icon src={Plus} alt="plus" onClick={() => arrayHelpers.insert(index, '')} />
                                            </ActionsControl>
                                        ))
                                    ) : (
                                            <button type="button" onClick={() => arrayHelpers.push('')}>
                                                Ajouter un titre alternatif
                                            </button>
                                        )}
                                    </>
                                )}
                            />
                        </FormControl>
                        <FormControl>
                            <Field name="year" placeholder="Année" label="Année" type="number" as={FormInput} />
                        </FormControl>
                        <FormControl>
                            <Field name="image" placeholder="Url de l'image" label="Affiche du film" type="input" as={FormInput} />
                        </FormControl>
                        <FormControl>
                            <Field name="color" placeholder="Couleur" label="Couleur" type="input" as={FormInput} />
                        </FormControl>
                    </LeftPanel>
                    <RightPanel>
                        <FormControl>
                            <Field name="score" placeholder="Score" label="Score" type="number" as={FormInput} />
                        </FormControl>
                        <FormControl>
                            <Field name="rating" placeholder="Rating" label="Rating" type="number" as={FormInput} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Acteurs</FormLabel>
                            <FieldArray
                                name="actors"
                                render={arrayHelpers => (
                                    <>
                                    {values.actors && values.actors.length > 0 ? (
                                        values.actors.map((actor, index) => (
                                            <ActionsControl key={index}>
                                                <Field name={`actors.${index}.actor`} placeholder="Acteur" type="input" as={FormInput} />
                                                <Field name={`actors.${index}.actorFacet`} placeholder="Url de l'image" type="input" as={FormInput} />
                                                <Icon src={Minus} alt="minus" onClick={() => arrayHelpers.remove(index)} />
                                                <Icon src={Plus} alt="plus" onClick={() => arrayHelpers.insert(index, '')} />
                                            </ActionsControl>
                                        ))
                                    ) : (
                                            <button type="button" onClick={() => arrayHelpers.push('')}>
                                                Ajouter un acteur
                                            </button>
                                        )}
                                    </>
                                )}
                            />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Genre</FormLabel>
                            <FieldArray
                                name="genre"
                                render={arrayHelpers => (
                                    <>
                                    {values.genre && values.genre.length > 0 ? (
                                        values.genre.map((genre, index) => (
                                            <ActionsControl key={index}>
                                                <Field name={`genre.${index}`} type="input" as={FormInput} />
                                                <Icon src={Minus} alt="minus" onClick={() => arrayHelpers.remove(index)} />
                                                <Icon src={Plus} alt="plus" onClick={() => arrayHelpers.insert(index, '')} />
                                            </ActionsControl>
                                        ))
                                    ) : (
                                            <button type="button" onClick={() => arrayHelpers.push('')}>
                                                Ajouter un genre
                                        </button>
                                        )}
                                    </>
                                )}
                            />
                        </FormControl>
                        <Button type="submit" disabled={isSubmitting}>Envoyer</Button>
                    </RightPanel>
                </FormWrapper>
            )}
        </Formik >
    );

};

export default FormPage;