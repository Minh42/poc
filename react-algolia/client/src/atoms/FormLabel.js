import styled from 'styled-components';

const FormLabel = styled.label`
    font-size: 1.2rem;
`;

export default FormLabel;