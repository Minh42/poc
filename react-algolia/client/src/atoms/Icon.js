import styled from 'styled-components';

const Icon = styled.img`
    height: 1.25rem;
    width: 1.25rem;
`

export default Icon;