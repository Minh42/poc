import React from 'react';
import styled from 'styled-components';
import FormLabel from './FormLabel';
import { useField } from "formik";
import colors from '../styles/colors';

const Input = styled.input`
    font-size: 1rem;
    font-family: inherit;
    color: ${colors.black};
    padding: 0.75rem 0.75rem;
    border-radius: 2px;
    background-color: rgba(${colors.divider}, .5);
    border: 1px solid ${colors.divider};
    display: block;
    transition: all .3s;
       
    &:focus {
        outline: none;
        box-shadow: 0 1rem 2rem rgba(${colors.black}, .1);
        border-bottom: 3px solid ${colors.primary};
    }

    &:focus:invalid {
        border-bottom: 3px solid ${colors.negativeRed};
    }

    &::-webkit-input-placeholder {
        color: ${colors.black};
    }
`

const Error = styled.p`
    color: ${colors.negativeRed};
`

const FormInput = ({
  label,
    ...props
}) => {
    const [field, meta] = useField(props);
    const errorText = meta.error && meta.touched ? meta.error : "";
    return (
        <>
        <FormLabel>{label}</FormLabel>
        <Input {...field} {...props} />
        <Error error="true">{errorText}</Error>
        </>
    );
};

export default FormInput;