import styled from 'styled-components';
import logoImg from '../assets/algolia-logo-white.png';

const Logo = styled.img.attrs({
    src: logoImg
}) `
width: 9rem;
height: auto;
`

export default Logo;
