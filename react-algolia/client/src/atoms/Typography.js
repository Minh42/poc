import styled from 'styled-components';
import colors from '../styles/colors';

const Typography = styled.h1`
    font-size: 4.5rem;
    color: ${colors.white};
    text-align: center;
`

export default Typography;
