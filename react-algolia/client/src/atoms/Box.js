import styled from 'styled-components';

const Box = styled.div`
    margin-top: 1rem;
    margin-bottom: 1rem;
`

export default Box;