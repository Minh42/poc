import styled from 'styled-components';
import colors from '../styles/colors';

const Button = styled.button`
    &,
    &:link,
    &:visited {
        text-transform: uppercase;
        text-decoration: none;
        padding: 1.5rem 4rem;
        display: inline-block;
        border-radius: 4px;
        transition: all .2s;
        border: none;
        cursor: pointer;
        background-color: ${colors.primary};
        color: ${colors.white};     
    }

    &:active,
    &:focus {
        outline: none;
        transform: translateY(-1px);
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.2); 
    }
`

export default Button;