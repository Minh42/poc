import React from 'react';
import StarRatings from 'react-star-ratings';


const Rating = ({ rating }) => {
    return (
        <StarRatings
            rating={rating}
            starRatedColor="gold"
            numberOfStars={5}
            name='rating'
            starDimension="1.25rem"
            starSpacing="0.1rem"
        />
    )
};

export default Rating;