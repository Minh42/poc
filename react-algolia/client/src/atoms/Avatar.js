import styled from 'styled-components';

const Avatar = styled.img`
    box-sizing: content-box;
    height: 2.5rem;
    width: 2.5rem;
    border-radius: 50%;
    border: 1.5px solid white;

    &:not(:last-child) {
        margin-right: -1rem;
    }
`

export default Avatar;